package main;

import java.util.ArrayList;
import java.util.Scanner;

import models.MiLista;

public class Main {

	public static void main(String[] args) {

		MiLista<Integer> c = new MiLista<>();
		
		print(c.size());
		c.clear();
		c.push(1);
		c.push(2);
		c.push(3);
		printCola(c);
		
		Integer ni = c.pop();
		print(ni);
		printCola(c);
		print(c.size());
		
		for (int i = 50; i < 54; i++)
			c.push(i);
		printCola(c);
		
		c.remove(52);
		if (!c.isEmpty())
			printCola(c);
		
		c.clear();
		if (c.isEmpty())
			printCola(c); //Muestra una cola vac�a
		
		c.push(54);
		if (c.contains(c.getItem(3))) //Devuelve null
			print(c.getItem(3));
		if (c.contains(c.getItem(0)))
				print(c.getItem(0));
		
		//Probando con LIFO
		print("PROBANDO CON LIFO");
		c.clear();
		c.setFIFO(false);
		for (int i = 0; i < 11; i++)
			c.push(i);
		printCola(c);
		c.pop();
		c.pop();
		printCola(c);
			
	}
	
	public static void print(Object o) {
		System.out.println("\nPrint-------------------------");
		System.out.println(o);
	}
	
	public static void printCola(MiLista<?> c){
		System.out.println("\nPrintCola-------------------------");
		for (int i = 0; i < c.size(); i++) {
			System.out.println(c.getItem(i));
		}
	}
}
