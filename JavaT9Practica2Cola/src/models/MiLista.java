package models;

//Debido a la modificaci�n del comentario de la asignaci�n, prefiero cambiar el nombre de la clase.
public class MiLista<T> {
	
	//ATRIBUTOS
	private Object[] miArray;
	private boolean isFIFO; //Por defecto ser� FIFO
	
	//CONSTRUCTOR
	/**
	 * Crea un nuevo objeto de tipo MiLista, con un tama�o de 0 y que se comporta como FIFO.
	 */
	public MiLista() {
		miArray = new Object[0];
		isFIFO = true;
	}

	//METODOS
	//GETTERS SETTERS
	
	/**
	 * Devuelve el tama�o de la lista.
	 * @return un entero con el tama�o de la lista.
	 */
	public int size(){ return miArray.length;}
	
	/**
	 * Devuelve true si la lista est� vac�a.
	 * @return true si la lista est� vac�a, de lo contrario false.
	 */
	public boolean isEmpty(){return (miArray.length == 0) ? true : false;}
	
	/**
	 * Devuelve true si la la lista es FIFO
	 * @return true si la la lista es FIFO, false si es LIFO
	 */
	public boolean isFIFO() { return isFIFO; }
	
	/**
	 * Cambia la funci�n de la lista seg�n el valor pasado por par�metros.
	 * @param isFIFO true si se desea que la lista sea FIFO, false si se desea LIFO.
	 */
	public void setFIFO (boolean isFIFO) { this.isFIFO = isFIFO; }
	
	/**
	 * Devuelve el item correspondiente con el index pasado por par�metros (si existe)
	 * (m�todo a�adido por mi cuenta)
	 * @param index el �ndice donde se encuentra el elemento
	 * @return el elemento a devolver, si existe.
	 */
	public T getItem (int index) {
		if (index <= miArray.length)
			return (T) miArray[index];
		else
			return null;
	}
	
	/**
	 * A�ade el elemento pasado por par�metros al final de la lista.
	 * @param elemento el elemento que se desea a�adir.
	 */
	public void push(T elemento) {
		Object[] nuevoArray = new Object[this.miArray.length+1];
		if (isFIFO) {
			nuevoArray[miArray.length] = elemento;
			//Relleno la lista de atr�s hacia delante
			for (int i = 0; i < miArray.length; i++)
				nuevoArray[i] = miArray[i];
		} else {
			nuevoArray[0] = elemento;
			//Relleno la lista de atr�s hacia delante, a partir del index 1 para la nueva lista
			for (int i = 0; i < miArray.length; i++)
				nuevoArray[i+1] = miArray[i];
		}
		miArray = nuevoArray;
	}
	
	/**
	 * Elimina de la lista el elemento pasado por par�metros.
	 * @param elemento el elemtento que se desea eliminar.
	 */
	public void remove(T elemento) {
		int posicion = -1;
		boolean encontrado = false;
		
		//Buscando si el elemento existe
		for (int i = 0; i < miArray.length && !encontrado; i++) {
			if (miArray[i].equals(elemento)){
				posicion = i;
				encontrado = true;
			}
		}
		
		//Si existe, se redimensiona el array y crea uno nuevo sin el elemento
		if (encontrado) {
			Object[] nuevoArray = new Object[miArray.length-1];
			for (int i = 0, j = i; i < miArray.length; i++, j++)
				if (i != posicion)
					nuevoArray[j] = miArray[i];
				else
					j--;
			miArray = nuevoArray;
		}
		//Me gustar�a poner un retorno de m�todo booleano, pero me ci�o a la especificaci�n
	}
	
	/**
	 * Elimina de la lista el siguiente elemento de la lista.
	 * @return el elemento eliminado de la lista.
	 */
	@SuppressWarnings("unchecked")
	public T pop(){
		T retorno = null;
		
		//Si el tama�o del array no es cero, devuelvo el primer elemento del valor de la lista
		if (miArray.length != 0) {
			if (isFIFO) {
				retorno = (T) miArray[miArray.length-1];
				Object[] nuevoArray = new Object[miArray.length-1];
				/*
				 * Copio en el nuevo array, el array antiguo, menos la �ltima posici�n
				 */
				for (int i = 0; i < miArray.length-1; i++)
					nuevoArray[i] = miArray[i];
				miArray = nuevoArray;
			} else {
				retorno = (T) miArray[0];
				Object[] nuevoArray = new Object[miArray.length-1];
				/*
				 * Empiezo a rellenar la lista a partir del segundo elemento de miArray (index 1)
				 * en la primera posici�n de nuevo array (index 1-1=0)
				 */
				for (int i = 1; i < miArray.length; i++)
					nuevoArray[i-1] = miArray[i];
				miArray = nuevoArray;
			}
		}
		return retorno;
	}
	
	/**
	 * Elimina de la lista todos los elementos y la deja con un tama�o de 0.
	 */
	public void clear(){
		miArray = new Object[0];
	}
	
	/**
	 * Devuelve true si el elemento pasado por par�metros se encuentra en la lista.
	 * @param o el elemento a buscar.
	 * @return el elemento encontrado, si existe.
	 */
	public boolean contains(T o){
		//Creo que es necesario el par�metro de tipo gen�rico (no est� en la especificaci�n)
		if (o != null)
			for (Object item : miArray) 
				if (item.equals(o))
					return true;
		return false;
	}
}
